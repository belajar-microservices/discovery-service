/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.belajar.marketplace.discoveryservice.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


/**
 *
 * @author opaw
 */
@Component
public class ScheduledTasks {
    
    private RestTemplate restTemplate = new RestTemplate();
        
    @Scheduled(fixedDelayString = "600000")
    public void keepConfigAlive() {
        String resp = restTemplate.getForObject("http://microservices-config.herokuapp.com/health", String.class);
        System.out.println(resp);
    }
}
